// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Quin',
  tagline: 'Virtual assistant for Neurospicy People',
  url: 'https://assistant-quin.gitlab.io',
  baseUrl: '/docs/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'Brocéliande Studio', // Usually your GitHub org/user name.
  projectName: 'Quin', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: '_',
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/assistant-quin/docs',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/assistant-quin/docs',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Quin',
        logo: {
          alt: 'Quin',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'goal',
            position: 'left',
            label: 'Project goal'
          },
          {
            type: 'doc',
            docId: 'roadmap',
            position: 'left',
            label: 'Roadmap'
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://gitlab.com/assistant-quin',
            label: 'GitLab',
            position: 'right',
          },
          {href: 'https://discord.gg/bKFsvtVa9h', label: 'Discord', position: 'right'},
        ],
      },
      footer: {
        style: 'dark',
        copyright: `CC BY SA ${new Date().getFullYear()} Brocéliande Studio. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
