---
sidebar_position: 4
---
# Pricing

Quin is not ready to be used yet, but this is the pricing plan we have decided on for when it will be ready.

## Self hosting

As stated before, the source code is open source and can be used for non commercial uses. Meaning a person or group of people can run the backend on their own at their own cost.

## User monthly fee

We provide it as a paid service for users. Our goal is to be able to sustain ourselves, the server costs and commission artists to design and animate the many bodies and outfits of Quin.

The first year, our goal is to cover server costs and be able to find our average cost per user. During that time, the pricing will be 10 USD per month or a reduced 100 USD for 12 months.

Nearing the end of each year we build our budget for the next year. If we can, we will lower the pricing to as low as 1 USD per month or 10 USD per 12 months. The budget plan is available publicly to explain our pricing as best we can.

## Donations

We accept donations and users can choose to pay more than the monthly fee.

## Giving back

If we have exceeding funds at the end of the year, we will grant yearly subscriptions to marginalized people who cannot afford the service.
