---
sidebar_position: 1
---

# End goal of Quin

## Concept statement

Quin is your own personal digital assistant gacha game that will help you manage your day to day life, keep track of the important tasks and your goals by harnessing the power of dopamine.

Quin has a customizable avatar who is interactive, gives you reminders and encouragement. You can use Quin to track appointments, plan your meals, remind you to drink water and take your medication, and so much more.

You'll be able to use Quin on both Desktop and Mobile. For those who wish to customize Quin further, the source code is available for free under a non commercial license.

We care about your privacy: your data is fully encrypted and you are the only one able to read it. Don't worry, you're still able to share some of it with people in your life to work on it as a team.

You don't know how to get started to keep a clean kitchen? Our templates are there to bootstrap your routines.

Finally, our collaborative cookbook makes building your ideal meals easy as it takes into account time, tools, spoons and dietary restrictions.

## Vision

Neurodivergent people struggle with executive functions especially in their daily life tasks.

We want to help them achieve their goals and live a fulfilling daily life.

## Aims

Existing tools often don't have a great user retention as they don't provide enough dopamine to keep the user coming back.

Our goal is to provide a tool that helps keep people on top of their life while being engaging enough that the user will make it a core part of their day.

## Detailed overview

Quin is a mobile and desktop application which provides the user with the tools to better manage their day to day life, it is primarily targeted at neurodivergent individuals.

### 1. Quin avatar

The name of the tool is the name of the assistant: Quin. They can be of any gender and their pronouns are they/them.

A starting avatar can be chosen among 3 then others are acquired as rewards. The user can browse through their available avatars and select the one that best suit them.

Each avatar has a set of default animations and several outfits. The alternative outfits are acquired as rewards and the user can choose which unlocked outfit Quin wears. Additional animations can be unlocked as reward for doing tasks in specific categories.

Each avatar has their own personality and dialogue lines.

### 2. Interactivity

#### a. chatting with Quin

The user has at their disposal a chat message system between them and Quin.

They are able to ask Quin to do different tasks like adding an item on a todo list, setting up a calendar event or picking a task to do.

#### b. interface

On the desktop application, the user has a toolbar linking to the main tools: ongoing day, week overview, calendar view, recipe book, contact book, backlog, projects and goal boards.

On the mobile application, they appear as items of a list that opens through a drawer.

### 3. Timeline

There is a color coded timeline of the day, to always be able to keep track of time and activities. This timeline is visible no matter what part of the application the user is in.

On the desktop application it appears horizontally. Hovering each upcoming section displays the information attached to it. The current activity is displayed in full with the time remaining displayed as a circle timer with the numerical value inside of it. The next activity is displayed in smaller text. There is 2 buttons: "I'm done" which marks the task as done and, depending on the time left, either launches the next activity or suggests to take a break ; "Stop" which opens a conversation with Quin to decide if the task needs to be postponed, needs more time or other possibilities.

On the mobile application, it appears vertically on the edge of the screen with the timer at its bottom. The next activity is not displayed. A widget of the timer is available to put on the home screen of phones and includes the full timer and next activity. A widget of the timeline is available to put on the home screen of phones and only includes the visual representation of the day.

### 4. Working space

This is the main view of the application. It shows a detailed view of the upcoming hour of the day. The background is specific to each Quin (for example their bedroom, flat, pirate ship or cottage)

Some rewards are animations of Quin in their environment (for example cooking, doing yoga or studying), others are improvements of the working space. The user gets to discover each avatar's tastes and story.

### 5. Time planification

The user can create recurring tasks and routines, which are successions of recurring tasks and time blocks, to plan their days quickly.

The time planification has 2 views: 8 upcoming days view and daily view.

The 8 days view show the time blocks and routines without details or specific times. The estimated spoons is displayed through color coding for easy visualization. The estimated time is displayed through the size of the blocks and space remaining in the day.

The daily view allows the user to organize their blocks on the timeline of the day and assign specific tasks to time blocks.

### 6. Calendar view

Quin can interface with .ics files to pull in calendar events. Each event offers the possibility to create a task to prepare for the event, create a task to do event follow up, but also to add time blocks for traveling time to and/or from the event.

Quin can also interface with your google and outlook account, which allows them to push specific time blocks onto the calendar.

### 7. Cookbook

The user can opt in to dietary restrictions including gluten free, lactose free, vegetarian, vegan and more. They can also fill in what ingredients and textures they don't like. Alternative ingredients and optional ingredients are proposed based on those restrictions.

Each recipe has the list of tools needed and the user can save what tool they own to easily filter the community recipes they can make. If they're missing a tool, they can add it to their to buy list easily. Alternative tools are suggested and adapt the steps, time and energy of the recipe accordingly.

Each recipe has the full list of tasks the user has to go through to complete it, including cleanup.

Users can suggest possible alternatives to existing recipes, create their own recipes and submit them to be publicly shared if they want to.

### 8. Contact book

The user can save their contacts and access any task related to them. They can save the frequency at each they wish to keep in touch. Those contacts will then be suggested during social time blocks.

Each contact has fields including address, birthday and other classic fields, but also dated notes to make it easy to keep track of events happening in each contact's life.

### 9. Tasks and projects

There are default task categories: self care, cooking, house chores, work, hobbies, family, social, health, administration.

Tasks have how many spoons they'll take the user, how long they're gonna take, dates, a full markdown note, recurrence, attached files,

Projects are sets of tasks, notes and files. Tasks groups can be done either in parallel or in sequence.

### 10. Goal boards

The user can create a vision board for each of their goals in the life categories. Putting in quotes and pictures or simple text.

Those boards will be displayed in the room during a time block associated with the life category.

### 11. Tokens and Quin customization

Each completed task gives tokens.

The user can see all their unlocked Quin avatars as cards. By increasing the level of the avatar, the user unlocks working space improvements and new costumes.

When the user selects an avatar, they can see the details of it.

They can loop through the different costumes, the locked ones are blacked out.

They can see the list of animations as well as their status (locked / unlocked) and how to unlock the locked ones. The unlocked animations can be played on the avatar viewer.

After the unlocked avatar cards are the locked ones.

On top of that screen is the roulette button, the user can spend tokens to get shards that can be used to increase the level of avatars or unlock new ones.

### 12. Collaboration

If one of your contacts has a Quin account, you can link it in their contact file. After approval by the contact you will be able to share tasks and projects with that contact, and they will be able to do the same with you.

Any task created by a contact goes through an approval step by the user before being added on their backlog.

### 13. Templates

Templates are sets of tasks that are shared publicly and can by saved on the user's account.

They can be straightforward or they can require some questions to be answered to customize. For example a pet care template would ask the name of the pet and would then add recurring tasks to buy pet food and a yearly vet appointment.

Once the template is applied it appears as a normal group of tasks in the category it's linked to and all its tasks can be modified according to the user's needs.

## Asset Creation

### Music

All the music in the application is background instrumental music to promote focus and relaxation. Each avatar has their playlist matching their personality.

### Art

Each avatar has their own art direction and color palette, for their body, environment and UI. However they all fall under the anime style umbrella.
