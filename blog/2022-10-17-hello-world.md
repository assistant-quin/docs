---
slug: hello-world
title: Hello world
authors: kit
tags: []
---

Finally making this project and sharing the process with the world.
So what is this project, who's Quin and why should you care about all of this?

<!-- truncate -->

## Hi, I'm not Quin!

Hi, my name is Kit, and I've been thinking about this project for several years now.

I am a fullstack engineer, mostly working on web projects. But I'm also autistic, ADHD and physically disabled.

I have struggled with daily life for figuratively all my existence:
- being late or way too early (and then late) to appointments
- forgetting half the things I needed to pack for school
- not reaching out to family members for months on end because time doesn't exist
- how do I never have clean clothes?
- what do you mean that was 3 months ago and not 3 weeks ago?
- walks to the fridge, opens the fridge, closes the fridge
- or flat out forget that eating and sleeping are things my body needs me to do

And the list goes on..

But also more recently on those kinds of topics:
- food intolerances
- money management
- taking care of a flat and a pet and a car and taxes
- making phone calls
- actually get personal projects done

I have tried all the tools I could get my hands on, but none of them had all the things, and none of them were enticing enough for my brain to latch on it long term.

## Who's Quin?

Quin will be a virtual assistant, to help me and people that share the same struggles, in a way that is compatible with how our bodies and brains work.

From daily rythm to tailoring a cookbook, scheduling time to send that birthday message, getting help implementing those long term goals, to instantly having the answer to "can I buy that thingy that costs X?"

With the encentive to have a conversational interface (and yes, waifus and husbandos to choose from, there, I said it)


---

So, if this is a tool that can be of use to you or someone you know, then I'm glad and I hope I'll actually achieve the goals I want to achieve!

The project is open source, with a CC BY SA license, and can be found on the links in the header and footer.

In the coming days I will be writing more about the features I intend to incorporate as well as the business model.
