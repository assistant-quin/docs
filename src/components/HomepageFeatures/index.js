import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Is adulting hard?',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        Quin is designed to fit our brains and low spoons. Its goal
        is to help you focus on what is important to you with as little stress
        as possible.
      </>
    ),
  },
  {
    title: 'Privacy first',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        Your data is your own and nobody should be able to read it without your
        consent. So nobody can. And you can read our source code to make sure of it.
      </>
    ),
  },
  {
    title: 'Accessibility as first class citizen',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Thought of by neurodivergent and disabled people, Quin is designed to
        be as accessible as possible. If something is missing, please let us know.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
